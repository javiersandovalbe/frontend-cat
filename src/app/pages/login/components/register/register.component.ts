import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder, 
    private loginService: LoginService, 
    private router: Router,
  ) {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      identification: ['', [Validators.required]],
      phone: ['', [Validators.required]]
    });
  }

  onSubmit(){
    if(this.registerForm.valid){
      this.loginService.register(this.registerForm.value).subscribe({
        next: (response) => {
          if(response.email){
            Swal.fire({
              title: 'Exito!',
              text: response?.email,
              icon: 'success',
              confirmButtonText: 'OK'
            })
            this.router.navigate(['login']);
          }
        },
        error: (e) => {
          console.error(e);
        }
      });
    }
  }
}
