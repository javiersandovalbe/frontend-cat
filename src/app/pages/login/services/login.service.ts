import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthLogin, UserLogin, User } from '../../../core/models/user.interface';
import { environment }from '../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  httpOptions!: HttpHeaders;
  url = environment.API_BACKEND;

  constructor(private httpClient: HttpClient) { 
    this.httpOptions = this.getHeaders();
  }

  private getHeaders(): HttpHeaders {
    let headers: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json;odata=nometadata',
    });
    return headers;
  }

  login(user: UserLogin): Observable<AuthLogin> {
    return this.httpClient.post<AuthLogin>(`${this.url}${environment.authEndpoint}/login`, user, { headers: this.httpOptions });
  }

  register(user: User): Observable<User> {
    return this.httpClient.post<User>(`${this.url}${environment.userEndpoint}/create`, user, { headers: this.httpOptions });
  }

}
