import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from './services/login.service';
import { Router } from '@angular/router';
import { StorageService } from '../../../app/core/services/storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  hide = true;
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder, 
    private loginService: LoginService, 
    private router: Router,
    private storageService: StorageService
  ) {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  clickEvent(event: MouseEvent) {
    this.hide = !this.hide;
    event.stopPropagation();
  }

  onSubmit(){
    if(this.loginForm.valid){
      this.loginService.login(this.loginForm.value).subscribe({
        next: (response) => {
          if(response.accessToken){
            this.storageService.save('accessToken',response.accessToken);
            this.router.navigate(['cat']);
          }
        },
        error: (e) => {
          console.error(e);
        }
      });
    }
  }
}
