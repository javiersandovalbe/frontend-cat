import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatComponent } from './cat.component';
import { InformationTableComponent } from 'src/app/shared/organisms/information-table/information-table.component';

const routes: Routes = [
  { 
    path: '',
    component: CatComponent,
    children: [
      { path: 'table', component: InformationTableComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatRoutingModule { }
