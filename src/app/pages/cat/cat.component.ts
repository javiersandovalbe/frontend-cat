import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CatService } from './services/cat.service';
import { Cat } from '../../core/models/cat.interface';
import { Image } from '../../core/models/image.interface';
import { ExchangeDataService } from '../../core/services/exchange-data.service';

@Component({
  selector: 'app-cat',
  templateUrl: './cat.component.html',
  styleUrls: ['./cat.component.scss']
})
export class CatComponent implements OnInit, OnDestroy{

  currentRoute!: string;
  totalCats!: number;
  cats: Cat[] = [];
  selectedCat!: Cat;
  imagesCat!: Image[];
  catSubscription! : Subscription;
  imageSubscription!: Subscription;

  constructor(
    private catService: CatService, 
    private router: Router,
    private dataService: ExchangeDataService
  ){}

  ngOnInit(): void {
    this.currentRoute = this.router.url;
    this.getPersonsData();
  }

  ngOnDestroy(): void {
    this.catSubscription.unsubscribe();
    this.imageSubscription.unsubscribe();
  }

  getPersonsData(){
    this.catSubscription = this.catService.findAll().subscribe({
      next: (value) =>{
        this.cats = value;
        this.dataService.sendData(value);
        this.totalCats = value.length;
      },
      error: (e) => {
        console.log('err', e);
      }
    })
  }

  getImagesCat(id: string){
    this.imageSubscription = this.catService.findImages(id).subscribe({
      next: (value) => {
        this.imagesCat = value;
      },
      error: (e) => {
        console.error(e);
      }
    })
  }

  listenCatSelected(id: string){
    this.selectedCat = this.filterCat(id);
    this.getImagesCat(id);
  }

  filterCat(id: string){
    const cat = this.cats.find(cat => cat.id === id);
    return cat ? cat : this.cats[0];
  }
}
