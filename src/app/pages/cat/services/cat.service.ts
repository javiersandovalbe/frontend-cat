import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cat } from '../../../core/models/cat.interface';
import { Image } from '../../../core/models/image.interface';
import { environment }from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { StorageService } from '../../../core/services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class CatService {

  httpOptions!: HttpHeaders;
  url = environment.API_BACKEND;

  constructor(private httpClient: HttpClient, private storageService: StorageService) { 
    this.httpOptions = this.getHeaders();
  }

  private getHeaders(): HttpHeaders {
    const accessToken = this.storageService.find('accessToken');
    let headers: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/json;odata=nometadata',
      'Authorization': `Bearer ${accessToken}`
    });
    return headers;
  }

  findAll(): Observable<Cat[]>{
    return this.httpClient.get<Cat[]>(`${this.url}${environment.catEndpoint}/breeds`, { headers: this.httpOptions });
  }

  findImages(id: string): Observable<Image[]>{
    return this.httpClient.get<Image[]>(`${this.url}${environment.imageEndpoint}/${id}`, { headers: this.httpOptions });
  }

}
