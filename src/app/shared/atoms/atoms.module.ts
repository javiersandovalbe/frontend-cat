import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleButtonComponent } from './simple-button/simple-button.component';
import { SimpleIconComponent } from './simple-icon/simple-icon.component';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [
    SimpleButtonComponent,
    SimpleIconComponent
  ],
  imports: [
    CommonModule,
    MatIconModule
  ],
  exports: [
    SimpleButtonComponent,
    SimpleIconComponent
  ]
})
export class AtomsModule { }
