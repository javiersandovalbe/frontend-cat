import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-simple-icon',
  templateUrl: './simple-icon.component.html',
  styleUrls: ['./simple-icon.component.scss']
})
export class SimpleIconComponent {
  @Input()
  icon!: string;
}
