import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AtomsModule } from '../atoms/atoms.module';
import { MoleculesModule } from '../molecules/molecules.module';
import { InformationTableComponent } from './information-table/information-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';


@NgModule({
  declarations: [
    InformationTableComponent
  ],
  imports: [
    CommonModule,
    AtomsModule,
    MoleculesModule,
    MatFormFieldModule,
    MatInputModule, 
    MatTableModule,
    MatSortModule, 
    MatPaginatorModule
  ],
  exports: [
    InformationTableComponent
  ]
})
export class OrganismsModule { }
