import { AfterContentInit, AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { Cat } from '../../../core/models/cat.interface';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ExchangeDataService } from '../../../core/services/exchange-data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-information-table',
  templateUrl: './information-table.component.html',
  styleUrls: ['./information-table.component.scss'],
})
export class InformationTableComponent implements OnInit{

  displayedColumns: string[] = ['id', 'name', 'origin', 'temperament', 'adaptability', 'child_friendly', 'intelligence', 'health_issues'];
  dataSource!: MatTableDataSource<Cat>;

  exchangeDataSubscription!: Subscription;

  cats!: Cat[];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private dataService: ExchangeDataService) {
  }
  ngOnInit(): void {
    this.getDataSource();
  }

  ngOnDestroy(): void {
    this.exchangeDataSubscription.unsubscribe();
  }

  getDataSource(){
    this.exchangeDataSubscription = this.dataService.data.subscribe(data => {
      this.cats = data;
      this.dataSource = new MatTableDataSource(this.cats);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
