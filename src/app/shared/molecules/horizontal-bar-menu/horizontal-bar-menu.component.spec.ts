import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HorizontalBarMenuComponent } from './horizontal-bar-menu.component';

describe('HorizontalBarMenuComponent', () => {
  let component: HorizontalBarMenuComponent;
  let fixture: ComponentFixture<HorizontalBarMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HorizontalBarMenuComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HorizontalBarMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
