import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AtomsModule } from '../atoms/atoms.module';
import { HorizontalBarMenuComponent } from './horizontal-bar-menu/horizontal-bar-menu.component';
import { SelectOptionsComponent } from './select-options/select-options.component';
import { IconCardComponent } from './icon-card/icon-card.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { ImageCardComponent } from './image-card/image-card.component';
import { ImageCarouselComponent } from './image-carousel/image-carousel.component';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { VoidFrameComponent } from './void-frame/void-frame.component';



@NgModule({
  declarations: [
    HorizontalBarMenuComponent,
    SelectOptionsComponent,
    IconCardComponent,
    ImageCardComponent,
    ImageCarouselComponent,
    VoidFrameComponent
  ],
  imports: [
    CommonModule,
    AtomsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    NgbCarouselModule
  ],
  exports: [
    HorizontalBarMenuComponent,
    SelectOptionsComponent,
    IconCardComponent,
    ImageCardComponent,
    ImageCarouselComponent,
    VoidFrameComponent
  ]
})
export class MoleculesModule { }
