import { Component, Input } from '@angular/core';
import { Image } from '../../../core/models/image.interface';
import { Cat } from '../../../core/models/cat.interface';

@Component({
  selector: 'app-image-carousel',
  templateUrl: './image-carousel.component.html',
  styleUrls: ['./image-carousel.component.scss']
})
export class ImageCarouselComponent {
  @Input()
  images: Image[] = [];

  @Input()
  cat!: Cat;
}
