import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-void-frame',
  templateUrl: './void-frame.component.html',
  styleUrls: ['./void-frame.component.scss']
})
export class VoidFrameComponent {

  @Input()
  src!: string;

  @Input()
  text!: string;
}
