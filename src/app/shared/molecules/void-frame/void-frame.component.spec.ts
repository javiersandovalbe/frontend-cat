import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VoidFrameComponent } from './void-frame.component';

describe('VoidFrameComponent', () => {
  let component: VoidFrameComponent;
  let fixture: ComponentFixture<VoidFrameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VoidFrameComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VoidFrameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
