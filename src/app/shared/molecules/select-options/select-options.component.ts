import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { ExchangeDataService } from '../../../core/services/exchange-data.service';
import { Subscription } from 'rxjs';
import { Cat } from '../../../core/models/cat.interface';

@Component({
  selector: 'app-select-options',
  templateUrl: './select-options.component.html',
  styleUrls: ['./select-options.component.scss']
})
export class SelectOptionsComponent implements OnInit, OnDestroy{

  @Output() emitChangeCat = new EventEmitter<string>();

  exchangeDataSubscription!: Subscription;
  cats: Cat[] = [];
  selectedCat!: string;

  constructor(private dataService: ExchangeDataService) {
  }

  ngOnInit(): void {
    this.getDataSource();
  }
  ngOnDestroy(): void {
    this.exchangeDataSubscription.unsubscribe();
  }

  getDataSource(){
    this.exchangeDataSubscription = this.dataService.data.subscribe(data => {
      this.cats = data;
    });
  }

  onChangeSelected(){
    this.emitChangeCat.emit(this.selectedCat);
  }

}
