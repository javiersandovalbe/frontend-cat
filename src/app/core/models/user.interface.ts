export interface User {
    email: string;
    password?: string;
    firstName: string;
    lastName: string;
    identification: string;
    phone: string;
}

export interface AuthLogin {
    message: string,
    accessToken: string
}

export interface UserLogin{
    email: string,
    password: string
}