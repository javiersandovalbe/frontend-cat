import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExchangeDataService {

  public data: BehaviorSubject<[]> = new BehaviorSubject([]);

  constructor() { }

  sendData(data: any): void{
    this.data.next(data);
  }
}
