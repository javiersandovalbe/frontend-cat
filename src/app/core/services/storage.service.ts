import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  public save(key: string, value: string){
    localStorage.setItem(key, value);
  }

  public find(key: string): any{
    return localStorage.getItem(key);
  }

  public remove(key: string){
    localStorage.removeItem(key);
    return true;
  }

  public clean(){
    localStorage.clear();
    return true;
  }
}
