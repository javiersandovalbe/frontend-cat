export const environment = {
    production: false,
    API_BACKEND: 'http://localhost:3000/api',
    catEndpoint: '/cat',
    imageEndpoint: '/image',
    userEndpoint: '/user',
    authEndpoint: '/auth'
}